package nomor2;

public class kedua {

	public static void main(String[] args) {
		 
        double diameter = 10;
        double jariJari = diameter / 2;
        double keliling = Math.PI * diameter;
        System.out.println("Keliling lingkaran dengan diameter " + diameter + " adalah " + keliling);

        
        double alas = 6;
        double tinggi = 8;
        double luas = 0.5 * alas * tinggi;
        System.out.println("Luas segitiga siku-siku dengan alas " + alas + " dan tinggi " + tinggi + " adalah " + luas);

      
        double diameterTabung = 5;
        double tinggiTabung = 10;
        double jariJariTabung = diameterTabung / 2;
        double volumeTabung = Math.PI * jariJariTabung * jariJariTabung * tinggiTabung;
        System.out.println("Volume tabung dengan diameter " + diameterTabung + " dan tinggi " + tinggiTabung + " adalah " + volumeTabung);
	}

}
