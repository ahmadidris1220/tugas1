package tugas2;

import java.util.Scanner;

public class person {
	// atribut dan method super class
	protected String name; 
	protected String address;
	protected String hobby;
	public void identity() {
	System.out.println("Nama: "+name); 
	System.out.println("Alamat: "+address);
	 System.out.println("hobby : " + hobby);
	}
	// inherit dari Person
	public class Student extends person{
	String nim;
	int jumlahSks;
	double hargaSpp;
	double hargaSks;
	double Modul;
	// method baru di subclass 
	public String getNim() {
	return nim;
	}
	 public Student(String name, String address, String nim, double hargaSpp, double hargaSks, double hargaModul, String hobby) {
	        this.name = name;
	        this.address = address;
	        this.nim = nim;
	        this.hargaSpp = hargaSpp;
	        this.hargaSks = hargaSks;
	        this.Modul = hargaModul;
	        this.hobby = hobby;
	    }

	
	public double hitungPembayaran() {
        Scanner scanner = new Scanner(System.in);

        // Memasukkan jumlah SKS
        System.out.print("Masukkan jumlah SKS: ");
        jumlahSks = scanner.nextInt();

        // Menghitung total pembayaran
        double total = (hargaSpp + (hargaSks * jumlahSks) + Modul);

        return total;
	}
	  public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        // Meminta input data mahasiswa
	        System.out.print("Masukkan nama mahasiswa: ");
	        String nama = scanner.nextLine();
	        System.out.print("Masukkan alamat mahasiswa: ");
	        String alamat = scanner.nextLine();
	        System.out.print("Masukkan NIM mahasiswa: ");
	        String nim = scanner.nextLine();

	        // Inisialisasi objek Student dengan input pengguna
	        Student mahasiswa = new Student(nama, alamat, nim, 500000, 100000, 50000);

	        // Memanggil method hitungPembayaran
	        double totalPembayaran = mahasiswa.hitungPembayaran();

	        // Menampilkan total pembayaran
	        System.out.println("Total pembayaran untuk " + mahasiswa.getName() + ": Rp " + totalPembayaran);
	    }
	}
	@Override
	public void identity()
	{
	super.identity();
	System.out.println("NIM: "+getNim());
	}
	}
	class InheritMain {
	public static void main(String[] args) {
		public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        // Input data mahasiswa
	        System.out.print("Masukkan nama mahasiswa: ");
	        String nama = scanner.nextLine();
	        System.out.print("Masukkan alamat mahasiswa: ");
	        String alamat = scanner.nextLine();
	        System.out.print("Masukkan NIM mahasiswa: ");
	        String nim = scanner.nextLine();

	        // Input data hobi
	        System.out.print("Masukkan hobi mahasiswa: ");
	        String hobi = scanner.nextLine();

	        // Membuat objek Student
	        Student mahasiswa = new Student(nama, alamat, nim, 500000, 100000, 50000);

	        // Memanggil method hitungPembayaran
	        double totalPembayaran = mahasiswa.hitungPembayaran();

	        // Menampilkan total pembayaran
	        System.out.println("Total pembayaran untuk " + mahasiswa.getName() + ": Rp " + totalPembayaran);

	        // Membuat objek Person
	        Person person = new Person(nama, 25, hobi);

	        // Memanggil method displayHobby
	        person.displayHobby();
	    }
	}
	}
}